jboss_eap_mssql_datasource_add
=========

Copy ms-sql jdbc dirver to EAP server

Call this role from main play:
=========

  roles:

    - role: jboss_eap_mssql_datasource_add
      mssql_datasource_name: "{{ eaps_mssql_datasource_name }}"
      mssql_connection: "{{ eaps_mssql_connection }}"
      mssql_username: "{{ eaps_mssql_username }}"
      mssql_password: "{{ eaps_mssql_password }}"
  
  vars_files:
    - config.yml
	
Here, the values are defined in group_vars of the main play under respective group name